# snmp-mibs-ietf conda recipe

Home: "https://tools.ietf.org/"

Recipe license: BSD 3-Clause

Summary: SNMP MIBs maintained by IETF
