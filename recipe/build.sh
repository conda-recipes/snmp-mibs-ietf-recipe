#!/bin/bash

mkdir -p ${PREFIX}/share/snmp/mibs
cp -Rf ietf ${PREFIX}/share/snmp/mibs

mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/snmp-mibs-ietf_activate.sh
IETFMIBDIR="${PREFIX}/share/snmp/mibs/ietf"
[  -z "\$MIBDIRS" ] && export MIBDIRS="\$IETFMIBDIR" || export MIBDIRS="\$MIBDIRS:\$IETFMIBDIR"
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/snmp-mibs-ietf_deactivate.sh
IETFMIBDIR="${PREFIX}/share/snmp/mibs/ietf"
export MIBDIRS=\$(echo -n \$MIBDIRS | tr ":" "\n" | grep -xv "\$IETFMIBDIR" | tr "\n" ":" | rev | cut -c 2- | rev)
EOF
